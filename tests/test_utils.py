#! -*- encoding: utf-8 -*-

import unittest

from geopaypy.utils import tools


class TestDatatools(unittest.TestCase):

    def test_tools(self):
        _dict = {
            "a": "foo",
            "b": "bar",
            "c": {
                "d": "Jhon",
                "e": "Doe"
            }
        }

        _str = tools.reduce_dict_to_str(
            _dict,
            sorted_by_keys=True
        )
        self.assertEqual(_str, 'afoobbarcdJhoneDoe')
        
        non_ordered_str = tools.reduce_dict_to_str(
            _dict,
            sorted_by_keys=False
        )
        #self.assertNotEqual(non_ordered_str, 'afoobbarcdJhoneDoe')

    def test_reduce_dict(self):
        _dict = {
            "az": "ValorAZ",
            "ab": "ValorAB",
            "c": 999,
            "bComplejo": {
                "b1": "ValorB1",
                "b3": "ValorB3",
                "b2": "Valor B2 ConCaractér.E$p"
            },
            "x": [
                {
                    "c": "ValorC1",
                    "a": "ValorA1"
                },
                {
                    "c": "ValorC2",
                    "a": "ValorA2"
                }
            ],
            "z": "ValorZ"
        }

        _str = tools.reduce_dict_to_str(
            _dict,
            sorted_by_keys=True
        )

        _str = tools.clean_special_characters(_str)

        reference = (
            "abValorABazValorAZbComplejob1ValorB1b2ValorB2ConCaractr.E$pb3ValorB3c999xaValorA1cValorC1aValorA2cValorC2zValorZ"
        )

        self.assertEqual(_str, reference)

    def test_clean_special_characters(self):
        _str = (
            "abValorABazValorAZbComplejob1ValorB1b2ValorB2 "
            "ConCaractér.E$pb3ValorB3c999xaValorA1cValorC1a"
            "ValorC2cValorC2zValorZ"
        )
        reference = (
            "abValorABazValorAZbComplejob1ValorB1b2"
            "ValorB2ConCaractr.E$pb3ValorB3c999xaVa"
            "lorA1cValorC1aValorC2cValorC2zValorZ"
        )

        cleaned = tools.clean_special_characters(_str)
        self.assertEqual(cleaned, reference)
