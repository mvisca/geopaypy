import unittest

import settings
from geopaypy.rsa import signers


class TestRSA(unittest.TestCase):

    def test_sign_and_verify_data(self):
        signer = signers.SHAWithRSA(
            private_key_path=settings.PRIVATE_KEY_PATH,
            public_key_path=settings.PUBLIC_KEY_PATH
        )

        data = "Cras sit amet orci pharetra"
        sign = signer.sign_data(data, "SHA")
        self.assertIsNotNone(sign, "The sign can't be None")
        verified = signer.verify_sign(sign, data, "SHA")
        self.assertTrue(verified)

        altered_data = data + "altered"
        verified = signer.verify_sign(sign, altered_data, "SHA")
        self.assertFalse(verified)




