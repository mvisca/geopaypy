import json
import unittest
from datetime import datetime

from geopaypy import __version__
from geopaypy.core import structures
from geopaypy.core.exceptions import EchoTestError
from geopaypy.core.client import Client, ClientKeys, ClientUrls


def test_version():
    assert __version__ == '0.1.0'


class TestStructures(unittest.TestCase):

    def setUp(self):
        from time import sleep
        sleep(1)

    def get_client(self):
        keys = ClientKeys(
            private_key_path="/home/me/Documentos/RSA/privatekey.pem",
            public_key_path="/home/me/Documentos/RSA/publickey.pem",
            geocom_public_key_path="/home/me/Sandbox/geopaypy/geopaypublickey.pem"
        )

        urls = ClientUrls(
            None,
            "https://geopaytest.geocom.com.uy:8099/geoswitchService/rest/process/payment",
            "https://geopaytest.geocom.com.uy:8099/geoswitchService/rest/process/paymentQuery",
            None,
            "https://geopaytest.geocom.com.uy:8099/geoswitchService/rest/process/echoTest"
        )

        return Client(
            urls,
            keys
        )

    def test_request_header(self):
        now = datetime.now()

        request_header = structures.RequestHeader(
            date_time=now,
            net_id="SLUCKIS_TEST",
            audit_number=now.strftime("%Y%m%d%H%M%S"),
            version="V114"
        )

        client = structures.Client(
            "51191634", "CI"
        )

        config = structures.Config(
            callback_url="https://sluckis.clk.com.uy/checkout/payment/done/"
        )

        request_payment = structures.RequestPayment(
            request_header,
            client,
            config,
        )

        address = structures.Address(
            country="URUGUAY",
            city="Montevideo",
            street="General Urquiza",
            door_number="2681 apto. 2"
        )

        invoice = structures.Invoice(
            final_consumer=True,
            serial="#3efw3",
            number="15422",
            date=datetime.now().date(),
            due_date=datetime.now().date()
        )

        invoice.set_amount(
            currency="UYU",
            total_amount=1.22
        )

        invoice.set_description(
            "Test payment request"
        )

        invoice.set_billing_address(
            address
        )

        request_payment.set_payment_info(
            "IVA",
            invoice,
            "ORD-14ax6"
        )

        ad = [
            {
                "name": "test",
                "value": "pepe"
            },
            {
                "name": "test",
                "value": "pepe"
            },
        ]

        request_payment.set_additional_data(
            shipping_address=address,
            ecommerce_additional_data=ad
        )

        request_payment.set_amounts(
            currency="UYU",
            amount=1.22,
            taxed_amount=1,
            tax_amount=0.22
        )

        _client = self.get_client()

        response = _client.request_payment(
            request_payment
        )

        self.assertTrue(response.ok)

    def test_echo_test(self):
        now = datetime.now()

        request_header = structures.RequestHeader(
            date_time=now,
            net_id="SLUCKIS_TEST",
            audit_number=now.strftime("%Y%m%d%H%M%S"),
            version="V114"
        )

        echo_test = structures.EchoTest(
            request_header
        )

        client = self.get_client()

        try:
            response = client.echo_test(
                echo_test
            )
            self.assertTrue(response.ok)
        except EchoTestError:
            pass

    def test_payment_query(self):
        now = datetime.now()

        request_header = structures.RequestHeader(
            date_time=now,
            net_id="SLUCKIS_TEST",
            audit_number=now.strftime("%Y%m%d%H%M%S"),
            version="V114"
        )

        payment_query = structures.PaymentQuery(
            request_header=request_header
        )
        payment_query.set_filter_options(
          access_token="bfcd8a57-1bd4-4c5d-83c7-ee5df4716b69"
        #   date_from=datetime(2020,4,15,16),
        #   date_to=datetime(2020,4,22,16)
        )

        client = self.get_client()
        response = client.payment_query(
            payment_query
        )

