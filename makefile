all: clean test

clean:
	find . -path "*.pyc" -delete

test:
	python -m unittest discover
