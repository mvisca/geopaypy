from importlib import import_module
from base64 import b64encode, b64decode

from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from Crypto.Signature.PKCS1_v1_5 import PKCS115_SigScheme


class SHAWithRSA:

    def __init__(self, private_key_path, public_key_path):

        self._private_key_path = private_key_path
        self._public_key_path = public_key_path

    def sign_data(self, data, sha_type):

        if not isinstance(data, bytes):
            data = data.encode("utf-8")

        key_file = open(self._private_key_path, "r").read()

        key_object = RSA.importKey(key_file)
        signer = PKCS1_v1_5.new(key_object)

        sha_type = import_module("Crypto.Hash.{}".format(sha_type))
        digest = sha_type.new()
        digest.update(data)

        sign = signer.sign(digest)
        return b64encode(sign)

    def verify_sign(self, sign, data, sha_type):
        if not isinstance(data, bytes):
            data = data.encode("utf-8")

        key_file = open(self._public_key_path, "r").read()
        key_object = RSA.importKey(key_file)
        verifier = PKCS115_SigScheme(key_object)

        sha_type = import_module("Crypto.Hash.{}".format(sha_type))
        digest = sha_type.new(data)



        verified = verifier.verify(digest, b64decode(sign))
        return verified

if __name__ == "__console__":
    data = "holacomoestas"
    signer = SHAWithRSA("./privatekey.pem", "./public_key.pem")
    sign = signer.sign_data(data, "SHA256")
    data = "holacomoestas."
    signer.verify_sign(sign, data, "SHA256")
