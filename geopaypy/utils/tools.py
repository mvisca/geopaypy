#! -*- encoding: utf-8 -*-
"""
.. module:: tools
   :platform: Unix
   :synopsis: A useful module indeed.

.. moduleauthor:: Mario A. Visca <mvisca89@gmail.com>
"""

import re
import stringcase


def reduce_dict_to_str(
    data_dict,
    sorted_by_keys=False,
    exclude_keys=None
):
    """
    Reduce a dict to a single string, where
    the keys and values are strings.
    This is a recursive function.

    :param data_dict: The data dict :p
    :type data_dict: Dict[str, str]

    :param sorted_by_keys: If you need consider the keys order
    :type sorted_by_keys: bool

    :param exclude_keys: List of keys to be excluded
    :type exclude_keys: List[str]

    :returns: str -- the reduced data_dict string

    Examples:
        >>> _dict = {
            "a": "foo",
            "b": "bar",
            "c":{
                "d": "John",
                "e": "Doe"
            }
        }

        >>> reduce_dict_to_str(_dict, sorted_by_keys=True)
        'afoobbarcdJhoneDoe'
    """
    _str = ""

    _keys = data_dict.keys()
    if sorted_by_keys:
        _keys = sorted(data_dict.keys(), key=lambda x: x.lower())

    for key in _keys:
        if exclude_keys and key in exclude_keys:
            continue

        value = data_dict[key]
        if isinstance(value, dict):
            value = reduce_dict_to_str(
                value,
                sorted_by_keys=sorted_by_keys,
                exclude_keys=exclude_keys
            )

        if isinstance(value, list):
            value = "".join(sorted([
                reduce_dict_to_str(
                    val,
                    sorted_by_keys=sorted_by_keys,
                    exclude_keys=exclude_keys
                ) for val in value
            ]))
        _str += u"{}{}".format(key, value)

    return _str


def clean_special_characters(value):
    reg_ex = re.compile(r"[^\x21-\x7E]")
    return reg_ex.sub("", value)


def parse_dict_keys(data_dict, format="snakecase"):
    parsed_dict = {}
    for k, v in data_dict.items():
        fn = getattr(stringcase, format)
        if isinstance(v, dict):
            v = parse_dict_keys(v, format=format)

        parsed_dict[fn(k)] = v

    return parsed_dict


def flatten(_dict, exclude=None, **kwargs):
    _list = []
    for k, v in _dict.items():
        if exclude and k in exclude:
            continue

        if isinstance(v, dict):
            v = flatten(v)

        if isinstance(v, list):
            v = [flatten(obj) for obj in v]

        _list.append({k: v})
    _list.sort(key=lambda k: k.keys()[0])
    return _list

def collapse(_list):
    _str = []
    for item in _list:
        if isinstance(item, list):
            _str.append(collapse(item))
        else:
            _key = item.keys()[0]
            _value = item[_key]
            if isinstance(_value, list):
                _value = collapse(_value)

            _str.append("{}{}".format(_key, _value))

    return "".join(_str)

# def reduce_dict_to_str(
#     data_dict,
#     sorted_by_keys=False,
#     exclude_keys=None
# ):
#     return collapse(flatten(data_dict, exclude=exclude_keys))

