import json
import requests

from requests_toolbelt.adapters import source

from . import structures
from geopaypy.rsa import signers
from geopaypy.utils import tools
from geopaypy.core.exceptions import EchoTestError


class ClientUrls:
    def __init__(
        self,
        payment_url,
        request_payment_url,
        payment_query_url,
        void_payment_url,
        echo_test_url
    ):
        self.payment = payment_url
        self.request_payment = request_payment_url
        self.payment_query = payment_query_url
        self.void_payment = void_payment_url
        self.echo_test = echo_test_url


class ClientKeys:

    def __init__(
        self,
        private_key_path,
        public_key_path,
        geocom_public_key_path
    ):

        self.private_key_path = private_key_path
        self.public_key_path = public_key_path
        self.geocom_public_key_path = geocom_public_key_path


class Client:

    def __init__(
        self,
        urls,
        keys,
        using_adapter_ip=None
    ):
        self.urls = urls
        self.signer = signers.SHAWithRSA(
            keys.private_key_path,
            keys.public_key_path
        )
        self.verifier = signers.SHAWithRSA(
            None,
            keys.geocom_public_key_path
        )

        self.client = requests.Session()
        if using_adapter_ip:
            new_source = source.SourceAddressAdapter(using_adapter_ip)
            self.client.mount('http://', new_source)
            self.client.mount('https://', new_source)

    def sign_data(self, data):
        sign_data = tools.reduce_dict_to_str(
            data,
            sorted_by_keys=True,
            exclude_keys=[]
        )

        sign_data = tools.clean_special_characters(sign_data).upper()
        sign = self.signer.sign_data(sign_data, "SHA")
        return sign

    def verify_response_sign(self, response_object, export=False):

        _data = tools.reduce_dict_to_str(
            response_object.serialize(),
            sorted_by_keys=True,
            exclude_keys=["digitalSign", ]
        )
        _data = tools.clean_special_characters(_data).upper()

        valid = self.verifier.verify_sign(
            response_object.response_header.digital_sign,
            _data,
            "SHA"
        )

        return valid

    def request_payment(
        self,
        request_payment_object
    ):
        if not isinstance(request_payment_object, structures.RequestPayment):
            raise ValueError(
                "Input must be a RequestPayment object"
            )

        request_payment_object.request_header.digital_sign = self.sign_data(
            request_payment_object.serialize()
        )

        _response = self.client.post(
            self.urls.request_payment,
            json=request_payment_object.serialize()
        )

        response = (
            structures
            .RequestPaymentResponse
            .new_from_response(_response.json())
        )
        is_authentic = self.verify_response_sign(response)
        if not is_authentic:
            raise Exception("The response isn't from GEOCOM")

        return response

    def echo_test(
        self,
        echo_test_object
    ):
        if not isinstance(echo_test_object, structures.EchoTest):
            raise ValueError(
                "Input must be a EchoTest object"
            )

        echo_test_object.request_header.digital_sign = self.sign_data(
            echo_test_object.serialize()
        )
        _response = self.client.post(
            self.urls.echo_test,
            json=echo_test_object.serialize()
        )

        try:
            response = (
                structures
                .EchoTestResponse
                .new_from_response(_response.json())
            )
            is_authentic = self.verify_response_sign(response)
            if not is_authentic:
                raise Exception("The response isn't from GEOCOM")

            return response
        except Exception:
            raise EchoTestError("Something was wrong")

    def payment_query(
        self,
        payment_query_object
    ):
        if not isinstance(payment_query_object, structures.PaymentQuery):
            raise ValueError(
                "Input must be a PaymentQuery object"
            )

        payment_query_object.request_header.digital_sign = self.sign_data(
            payment_query_object.serialize()
        )
        _response = self.client.post(
            self.urls.payment_query,
            json=payment_query_object.serialize()
        )

        response = (
            structures
            .PaymentQueryResponse
            .new_from_response(_response.json())
        )

        is_authentic = self.verify_response_sign(response, export=True)
        if not is_authentic:
            raise Exception("The response isn't from GEOCOM")

        return response

