from decimal import Decimal

from .exceptions import ValidationError


class Field(object):

    def __init__(self, required=False):
        self.required = required
    
    def validate(self, value):
        if not value and self.required:
            raise ValidationError("This field is required")

    def parse(self, value):
        return value
        

class TextField(Field):
    
    def __init__(self, min_length=None, max_length=None, **kwargs):
        super(TextField, self).__init__(**kwargs)
        self.min_length = min_length
        self.max_length = max_length

    def _validate_min_length(self, value):
        if self.min_length and len(value) < self.min_length:
            raise ValidationError(
                "This field value must be at "
                "least {} characters long.".format(self.min_length)
            )
        return True

    def _validate_max_length(self, value):
        if self.max_length and len(value) > self.max_length:
            raise ValidationError(
                "This field value must have less "
                "than equal {} characters.".format(self.max_length)
            )
        return True

    def validate(self, value):
        super(TextField, self).validate(value)
        self._validate_max_length(value)
        self._validate_min_length(value)

        return True


class EnumField(Field):

    def __init__(self, enum, **kwargs):
        super(EnumField, self).__init__(**kwargs)
        self.enum = enum
    
    def _validate_enum_value(self, value):
        if value not in self.enum.__members__:
            raise ValidationError(
                "Invalid field value. The available values are: \n"
                "{}".format(", ".join(self.enum.available_values()))
            )
        return True
    
    def validate(self, value):
        super(EnumField, self).validate(value)
        self._validate_enum_value(value)

    def parse(self, value):
        return self.enum[value].value


class AmountField(Field):

    def __init__(self, **kwargs):
        super(AmountField, self).__init__(**kwargs)
    
    def parse(self, value):
        _value = Decimal(value).quantize(Decimal("0.01"))
        _value = _value.to_eng_string().replace(".", "")
        return _value


class DateField(Field):

    def __init__(self, **kwargs):
        super(DateField, self).__init__(**kwargs)

    def parse(self, value):
        _value = value.strftime("%Y-%m-%d")
        return _value


class DateTimeField(Field):

    def __init__(self, **kwargs):
        super(DateTimeField, self).__init__(**kwargs)

    def parse(self, value):
        _value = value.strftime("%Y-%m-%d %H:%M:%S")
        return _value


class SchemaField(Field):

    def __init__(self, schema, **kwargs):
        super(SchemaField, self).__init__(**kwargs)
        self.schema = schema

    def parse(self, value):
        return self.schema.serialize(value.__dict__)


class SchemaListField(Field):

    def __init__(self, schema, **kwargs):
        super(SchemaListField, self).__init__(**kwargs)
        self.schema = schema



