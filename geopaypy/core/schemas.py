import stringcase

from .fields import (
    Field,
    TextField,
    EnumField,
    DateField,
    AmountField,
    SchemaField,
    DateTimeField,
    SchemaListField
)
from .enums import (
    IndiEnum,
    VersionEnum,
    PreAuthEnum,
    CountryEnum,
    CurrencyEnum,
    DocumentTypeEnum,
    PaymentStateEnum
)
from .exceptions import ValidationError

import wrapt


class Schema(object):

    key_parser = 'camelcase'

    def validate_fields(self, fields_dict, must_raise=True):
        for field_name, value in fields_dict.items():
            _field = getattr(self, field_name)
            try:
                _field.validate(value)
            except ValidationError as err:
                if must_raise:
                    raise type(err)("{}: {}".format(field_name, err.message))
                continue
    
    # def validate_inputs(self, func):
    #     func_args_names = [
    #         name for name in func.func_code.co_varnames if name != "self"
    #     ]

    #     def _inner(*args, **kwargs):
            
    #         fields_dict = {
    #             k: v for k, v in zip(func_args_names, args)
    #         }
    #         fields_dict.update(kwargs)
            
    #         # import pdb; pdb.set_trace()
    #         try:
    #             self.validate_fields(fields_dict=fields_dict)
    #         except ValidationError as err:
    #             raise err

    #         return func(*args, **kwargs)

    #     return _inner

    @wrapt.decorator
    def validate_inputs(self, wrapped, instance, args, kwargs):
        func_args_names = [
            name for name in wrapped.func_code.co_varnames if name != "self"
        ]

        fields_dict = {
            k: v for k, v in zip(func_args_names, args)
        }
        fields_dict.update(kwargs)
        
        try:
            self.validate_fields(fields_dict=fields_dict)
        except ValidationError as err:
            raise err

        return wrapped(*args, **kwargs)
    
    @classmethod
    def serialize(cls, data_dict):
        _serialized_data = {}
        
        for k, v in data_dict.items():
            if v is None:
                continue

            _field = getattr(cls, k)
            _key_parser = getattr(stringcase, cls.key_parser)
            _serialized_data[_key_parser(k)] = _field.parse(v)

        return _serialized_data


class ClientSchema(Schema):

    client_id_type = TextField(
        required=True, max_length=50
    )

    client_id = TextField(
        required=True,
        max_length=50
    )

    email = Field(
        required=False
    )

    mobile = Field(
        required=False
    )

    first_name = Field(
        required=False
    )

    last_name = Field(
        required=False
    )
    
    document_type = EnumField(
        required=False,
        enum=DocumentTypeEnum
    )

    document_value = Field(
        required=False
    )


class InvoiceLineSchema(Schema):

    order = TextField(
        required=True,
        max_length=2
    )

    detail = TextField(
        required=True,
        max_length=30
    )

    amount = AmountField(
        required=None
    )


class AddressSchema(Schema):

    country = EnumField(
        required=True,
        enum=CountryEnum
    )

    city = TextField(
        required=True,
    )

    street = TextField(
        required=True
    )

    door_number = TextField(
        required=True
    )

    zip_code = TextField(
        required=False
    )


class AdditionalDataSchema(Schema):

    name = TextField(
        required=False,
        max_length=50
    )
    
    value = TextField(
        required=False,
        max_length=50
    )


class InvoiceSchema(Schema):

    final_consumer = Field(
        required=True
    )

    serial = Field(
        required=True
    )

    number = TextField(
        required=True,
        max_length=10
    )

    total_amount = AmountField(
        required=True
    )

    currency = EnumField(
        required=True,
        enum=CurrencyEnum
    )

    date = DateField(
        required=True
    )

    due_date = DateField(
        required=True
    )

    description = TextField(
        required=True
    )

    lines = SchemaListField(
        required=False,
        schema=InvoiceLineSchema
    )

    address = SchemaField(
        required=True,
        schema=AddressSchema
    )


class RequestHeaderSchema(Schema):

    date_time = DateTimeField(
        required=True,
    )

    net_id = TextField(
        required=True,
        max_length=20
    )

    net_decription = TextField(
        required=False
    )

    audit_number = TextField(
        required=True,
        max_length=16
    )

    version = EnumField(
        required=True,
        enum=VersionEnum
    )

    additional_data = SchemaListField(
        required=False,
        schema=AdditionalDataSchema
    )

    digital_sign = TextField(
        required=True
    )


class ResponseHeaderSchema(Schema):
    response_code = TextField(
        required=True,
    )

    response_description = TextField(
        required=False
    )

    rrn = TextField(
        required=False
    )

    additional_data = SchemaListField(
        required=False,
        schema=AdditionalDataSchema
    )

    digital_sign = TextField(
        required=True,
    )


class ConfigSchema(Schema):

    css_file_name = TextField(
        required=False
    )

    prop_file_name = TextField(
        required=False
    )

    callback_url = TextField(
        required=True
    )

    callback_url_suffix = TextField(
        required=False
    )

    url_update_info = TextField(
        required=False
    )

    update_info_required = Field(
        required=False
    )

    enabled_until = DateTimeField(
        required=False
    )

    count_uses = Field(
        required=False
    )

    show_save_card_option = Field(
        required=False
    )

    hide_cards = Field(
        required=False
    )

    hide_banks = Field(
        required=False
    )

    hide_PayNet = Field(
        required=False
    )

    payment_token_enabled_until = DateTimeField(
        required=False
    )


class RequestPaymentSchema(Schema):

    request_header = SchemaField(
        required=True,
        schema=RequestHeaderSchema
    )

    merchant_id = TextField(
        required=False
    )

    currency = EnumField(
        required=True,
        enum=CurrencyEnum
    )

    amount = AmountField(
        required=True
    )

    tip_amount = AmountField(
        required=False
    )

    taxed_amount = AmountField(
        required=True
    )

    tax_amount = AmountField(
        required=True
    )

    indi = EnumField(
        required=True,
        enum=IndiEnum
    )

    installments = TextField(
        required=False,
        max_length=2
    )

    preauthorization = EnumField(
        required=False,
        enum=PreAuthEnum
    )

    invoice = SchemaField(
        required=True,
        schema=InvoiceSchema
    )

    reference = TextField(
        required=True,
        max_length=50
    )

    shipping_address = SchemaField(
        required=False,
        schema=AddressSchema
    )

    client = SchemaField(
        required=True,
        schema=ClientSchema
    )

    config = SchemaField(
        required=True,
        schema=ConfigSchema
    )

    ecommerce_additional_data = SchemaListField(
        required=False,
        schema=AdditionalDataSchema
    )

    authorizer_additional_data = SchemaListField(
        required=False,
        schema=AdditionalDataSchema
    )


class RequestPaymentResponseSchema(Schema):

    response_header = SchemaField(
        required=True,
        schema=ResponseHeaderSchema
    )

    token = TextField(
        required=False
    )


class CardSchema(Schema):

    card_token = TextField(
        required=True
    )

    card_mask = TextField(
        required=True
    )

    bin = TextField(
        required=False
    )

    card_brand_code = TextField(
        required=True
    )

    card_brand_name = TextField(
        required=True
    )

    issuer_code = TextField(
        required=False
    )

    issuer_name = TextField(
        required=False
    )

    program_code = TextField(
        required=False
    )

    program_name = TextField(
        required=False
    )

    international = TextField(
        required=False
    )

    type = TextField(
        required=False
    )

    due_date = TextField(
        required=False
    )


class AuthorizerDataSchema(Schema):

    merchant_code = TextField(
        required=False
    )

    terminal_code = TextField(
        required=False
    )

    response__code = TextField(
        required=False
    )

    response_description = TextField(
        required=False
    )

    authorization_code = TextField(
        required=False
    )

    reference_request = TextField(
        required=False
    )

    reference_response = TextField(
        required=False
    )

    batch_number = TextField(
        required=False
    )

    ticket_number = TextField(
        required=False
    )

    code = TextField(
        required=False
    )

    additional_data = SchemaField(
        required=False,
        schema=AdditionalDataSchema
    )


class PaymentSchema(Schema):

    payment_token = TextField(
        required=True,
    )

    access_token = TextField(
        required=False
    )

    date_time = DateTimeField(
        required=True
    )

    state = EnumField(
        required=True,
        enum=PaymentStateEnum
    )

    paymentmode = TextField(
        required=True
    )

    payment_entity = TextField(
        required=True
    )

    authorized = Field(
        required=False
    )

    voided = Field(
        required=False
    )

    refunded = Field(
        required=False
    )

    client = SchemaField(
        required=False,
        schema=ClientSchema
    )

    card = SchemaField(
        required=False,
        schema=CardSchema
    )

    currency = TextField(
        required=True
    )

    amount = AmountField(
        required=True
    )

    tip_amount = AmountField(
        required=False
    )

    taxed_amount = AmountField(
        required=False
    )

    tax_amount = AmountField(
        required=False
    )

    refund_amount = AmountField(
        required=False
    )

    indi = Field(
        required=True
    )

    invoice = SchemaField(
        required=False,
        schema=InvoiceSchema
    )

    plan = TextField(
        required=False,
    )

    installments = TextField(
        required=True
    )

    iva_discount_amount = AmountField(
        required=False,
    )

    geopay_merchant_id = TextField(
        required=True
    )

    authorizer = SchemaField(
        required=False,
        schema=AuthorizerDataSchema
    )

    preauthorized = Field(
        required=False
    )

    additional_data = SchemaListField(
        required=False,
        schema=AdditionalDataSchema
    )


class SecurityCodeSchema(Schema):

    type = TextField(
        required=True
    )

    value = TextField(
        required=True
    )


class PaymentResponseSchema(Schema):

    response_header = SchemaField(
        required=True,
        schema=ResponseHeaderSchema
    )

    token = TextField(
        required=True
    )

    reference = TextField(
        required=False
    )

    payment = SchemaField(
        required=False,
        schema=PaymentSchema
    )

    ecommerce_additional_data = SchemaListField(
        required=False,
        schema=AdditionalDataSchema
    )

    additional_data = SchemaListField(
        required=False,
        schema=AdditionalDataSchema
    )

    security_code = SchemaListField(
        required=False,
        schema=SecurityCodeSchema
    )


class PaymentQuerySchema(Schema):

    request_header = SchemaField(
        required=True,
        schema=RequestHeaderSchema
    )

    access_token = TextField(
        required=False
    )

    date_from = DateTimeField(
        required=False
    )

    date_to = DateTimeField(
        required=False
    )

    merchant_id = TextField(
        required=False,
    )

    first_id = Field(
        required=False
    )


class PaymentQueryResponseSchema(Schema):

    response_header = SchemaField(
        required=True,
        schema=ResponseHeaderSchema
    )

    payments = SchemaListField(
        required=False,
        schema=PaymentSchema
    )

    quantity = Field(
        required=False
    )

    has_more = Field(
        required=False
    )

    first_id = Field(
        required=False
    )

    last_id = Field(
        required=False
    )


class EchoTestSchema(Schema):

    request_header = SchemaField(
        required=True,
        schema=RequestHeaderSchema
    )


class EchoTestResponseSchema(Schema):

    response_header = SchemaField(
        required=True,
        schema=ResponseHeaderSchema
    )

class CardsRequestSchema(Schema):
    pass