#! -*- encoding: utf-8 -*-


CODE_PA = (
    u"Se solicitó pre-autorización y fue autorizada por el sello."
    u"El comercio debe confirmar la compra para que sea efectiva"
)

CODE_AN = (
    u"Procesada localmente por GEOPay. Servicio de pre-autorización "
    u"no disponible para el sello seleccionado. Nose pudo ejecutar "
    u"contra el autorizador la pre-autorización de la compra."
    u"El comercio debe decidir si permite continuar con la venta, si decide "
    u"continuar debe enviar 'confirmar pago', en ese momento se solicitará la compra al autorizador"
)

CODE_RPA = (
    u"Pre-autorización rechazada por el sello"
)

CODE_05 = (
    u"Transacción denegada"
)

CODE_06 = (
    u"Código de verificación inválido"
)

CODE_07 = (
    u"Error en notificación"
)

CODE_25 = (
    u"Pago no encontrado"
)

CODE_30 = (
    u"Error en mensaje"
)

CODE_92 = (
    u"Sin conexión con el autorizador del medio de pago"
)

CODE_93 = (
    u"Se superó el tiempo de espera por respuesta del "
    u"autorizdor del medio de pago. En este caso no se tiene "
    u"conocimiento del resultado en el autorizador, se deben tomar "
    u"acciones administrativas o ejecutar 'Consulta de Pago' hasta "
    u"obtener como resultado el estado 'PROCESSED'"
)

CODE_94 = (
    u"Número de auditoria duplicado"
)

CODE_RE = (
    u"Anulación - Compra aún en proceso, reintente."
)

CODE_IF01 = (
    u"Solicitar pagina de compra - cancelada por el usuario"
)

CODE_99 = (
    u"Rechazo genérico"
)

INFO_CODES = {
    "00": u"OK",
    "PA": CODE_PA,
    "AN": CODE_AN,
    "RPA": CODE_RPA,
    "05": CODE_05,
    "06": CODE_06,
    "07": CODE_07,
    "25": CODE_25,
    "30": CODE_30,
    "92": CODE_92,
    "93": CODE_93,
    "94": CODE_94,
    "RE": CODE_RE,
    "IF01": CODE_IF01,
    "99": CODE_99,
}