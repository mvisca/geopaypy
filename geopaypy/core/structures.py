from . import schemas
from . import info_texts
from geopaypy.utils import tools


class Base(object):

    schema = None

    def serialize(self):
        if not self.schema:
            raise AttributeError("A schema is required to perfom this method")
        
        serialized_data = self.schema.serialize(self.__dict__)
        return serialized_data


class Client(Base):

    schema = schemas.ClientSchema()

    @schema.validate_inputs
    def __init__(self, client_id, client_id_type):
        self.client_id = client_id
        self.client_id_type = client_id_type

    @schema.validate_inputs
    def set_client_contact_info(self, email=None, mobile=None):
        self.email = email
        self.mobile = mobile
    
    @schema.validate_inputs
    def set_client_personal_info(
        self,
        first_name=None,
        last_name=None,
        document_type=None,
        document_value=None
    ):
        self.first_name = first_name
        self.last_name = last_name
        self.document_type = document_type
        self.document_value = document_value


class RequestHeader(Base):

    schema = schemas.RequestHeaderSchema()

    @schema.validate_inputs
    def __init__(
        self,
        date_time,
        net_id,
        audit_number,
        version,
        net_description=None
    ):
        self.date_time = date_time
        self.net_id = net_id
        self.audit_number = audit_number
        self.version = version
        self.net_description = net_description
    
    @schema.validate_inputs
    def set_additional_data(
        self,
        additional_data
    ):
        """
         Set the additional_data
        """
        self.additional_data = additional_data
    
    def sign_data(self, signer):
        self.sign_data = "asdasd"


class Config(Base):

    schema = schemas.ConfigSchema()

    @schema.validate_inputs
    def __init__(
        self,
        callback_url,
    ):
        self.callback_url = callback_url

    @schema.validate_inputs
    def set_css_and_props(
        self,
        css_file_name=None,
        prop_file_name=None
    ):
        self.css_file_name = css_file_name
        self.prop_file_name = prop_file_name

    @schema.validate_inputs
    def set_urls_options(
        self,
        callback_url_suffix=None,
        url_update_info=None,
        update_info_required=None
    ):
        self.callback_url_suffix = callback_url_suffix
        self.url_update_info = url_update_info
        self.update_info_required = update_info_required

    @schema.validate_inputs
    def set_card_options(
        self,
        enabled_until=None,
        count_uses=None,
        show_save_card_option=None,
    ):
        self.enabled_until = enabled_until
        self.count_uses = count_uses
        self.show_save_card_option = show_save_card_option

    @schema.validate_inputs
    def set_hideable_options(
        self,
        hide_cards=None,
        hide_banks=None,
        hide_PayNet=None
    ):
        self.hide_cards = hide_cards
        self.hide_banks = hide_banks
        self.hide_PayNet = hide_PayNet

    @schema.validate_inputs
    def set_token_options(
        self,
        payment_token_enabled_until=None,
    ):
        self.payment_token_enabled_until = payment_token_enabled_until
    
    @schema.validate_inputs
    def set_redirect_options(
        self,
        use_redirect=None
    ):
        self.use_redirect = use_redirect


class Invoice(Base):

    schema = schemas.InvoiceSchema()

    @schema.validate_inputs
    def __init__(
        self,
        final_consumer,
        serial,
        number,
        date,
        due_date
    ):

        self.final_consumer = final_consumer
        self.serial = serial
        self.number = number
        self.date = date
        self.due_date = due_date
    
    @schema.validate_inputs
    def set_amount(
        self,
        currency,
        total_amount
    ):
        self.currency = currency
        self.total_amount = total_amount
    
    @schema.validate_inputs
    def set_description(
        self,
        description
    ):
        self.description = description
    
    @schema.validate_inputs
    def set_billing_address(
        self,
        address
    ):
        self.address = address

    @schema.validate_inputs
    def set_invoice_lines(
        self,
        lines
    ):
        self.lines = lines


class Address(Base):

    schema = schemas.AddressSchema()

    @schema.validate_inputs
    def __init__(
        self,
        country,
        city,
        street,
        door_number,
        zip_code=None
    ):
        self.country = country
        self.city = city
        self.street = street
        self.door_number = door_number
        self.zip_code = zip_code


class RequestPayment(Base):

    schema = schemas.RequestPaymentSchema()

    @schema.validate_inputs
    def __init__(
        self,
        request_header,
        client,
        config,
        merchant_id=None
    ):
        """
        Init
        """
        self.request_header = request_header
        self.client = client
        self.config = config
        self.merchant_id = merchant_id

    @schema.validate_inputs
    def set_payment_info(
        self,
        indi,
        invoice,
        reference,
        installments=None,
        preauthorization=None,
    
    ):
        """
        Set the all related payment info
        """

        self.indi = indi
        self.installments = installments
        self.preauthorization = preauthorization
        self.invoice = invoice
        self.reference = reference

    @schema.validate_inputs
    def set_additional_data(
        self,
        shipping_address=None,
        ecommerce_additional_data=None,
        authorizer_additional_data=None
    ):
        self.shipping_address = shipping_address
        self.ecommerce_additional_data = ecommerce_additional_data
        self.authorizer_additional_data = authorizer_additional_data

    @schema.validate_inputs
    def set_amounts(
        self,
        currency,
        amount,
        taxed_amount,
        tax_amount,
        tip_amount=None,
    ):
        self.currency = currency
        self.amount = amount
        self.tip_amount = tip_amount
        self.taxed_amount = taxed_amount
        self.tax_amount = tax_amount


class ResponseHeader(Base):

    schema = schemas.ResponseHeaderSchema()

    @schema.validate_inputs
    def __init__(
        self,
        digital_sign,
        response_code,
        rrn=None,
        additional_data=None,
        response_description=None,
    ):
        self.rrn = rrn
        self.digital_sign = digital_sign
        self.response_code = response_code
        self.response_description = response_description
        self.additional_data = additional_data

    def get_code_description(self):
        return info_texts.INFO_CODES.get(
            self.response_code, u"Generic Rejection"
        )


class ResponseBase(Base):

    @classmethod
    def new_from_response(cls, response_data):
        _data = tools.parse_dict_keys(response_data)
        response_header = ResponseHeader(**_data.get("response_header"))
        token = _data.get("token", None)
        return cls(response_header, token)

    @property
    def ok(self):
        if self.response_header.response_code not in ["00", "OK"]:
            return False
        return True

    @property
    def code(self):
        return self.response_header.response_code


    @property
    def description(self):
        return self.response_header.response_description
    
    @property
    def code_description(self):
        return self.response_header.get_code_description()


class RequestPaymentResponse(ResponseBase):

    schema = schemas.RequestPaymentResponseSchema()

    @schema.validate_inputs
    def __init__(
        self,
        response_header,
        token 
    ):
        self.response_header = response_header
        self.token = token

    @classmethod
    def new_from_response(cls, response_data):
        _data = tools.parse_dict_keys(response_data)
        response_header = ResponseHeader(**_data.get("response_header"))
        token = _data.get("token", None)
        return cls(response_header, token)


class AuthorizerData(Base):

    schema = schemas.AuthorizerDataSchema()

    def __init__(self, **kwargs):

        for k, v in kwargs.items():
            setattr(k, v)


class Card(Base):

    schema = schemas.CardSchema()

    @schema.validate_inputs
    def __init__(
        self,
        card_token,
        card_mask,
        card_brand_code,
        card_brand_name,
        bin=None
    ):
        self.card_token = card_token
        self.card_mask = card_mask
        self.card_brand_code = card_brand_code
        self.card_brand_name = card_brand_name
        self.bin = bin
    
    @schema.validate_inputs
    def set_issuer_info(self, issuer_code, issuer_name):
        self.issuer_code = issuer_code
        self.issuer_name = issuer_name

    @schema.validate_inputs
    def set_program_info(self, program_code, program_name):
        self.program_code = program_code
        self.program_name = program_name

    @schema.validate_inputs
    def set_card_info(self, internation, type, due_date):
        self.international = internation
        self.type = type
        self.due_date = due_date


class Payment(Base):

    schema = schemas.PaymentSchema()

    @schema.validate_inputs
    def __init__(
        self,
        payment_token,
        date_time,
        state,
        geopay_merchant_id,
        access_token=None,
        preauthorized=None
    ):
        self.payment_token = payment_token
        self.date_time = date_time
        self.state = state
        self.access_token = access_token
        self.preauthorized = preauthorized
        self.geopay_merchant_id = geopay_merchant_id

    @schema.validate_inputs
    def set_payment_info(
        self,
        paymentmode,
        payment_entity,
        authorizer=None,
        authorized=None,
        voided=False,
        refunded=False,
    ):
        self.paymentmode = paymentmode
        self.payment_entity = payment_entity
        self.authorized = authorized
        self.voided = voided
        self.authorizer = authorizer
        self.refunded = refunded
    
    @schema.validate_inputs
    def set_client(self, client):
        self.client = client
    
    @schema.validate_inputs
    def set_card(self, card):
        self.card = card

    @schema.validate_inputs
    def set_amounts(
        self,
        currency,
        amount,
        tax_amount=None,
        tip_amount=None,
        taxed_amount=None,
        refund_amount=None,
    ):
        self.currency = currency
        self.amount = amount
        self.tax_amount = tax_amount
        self.tip_amount = tip_amount
        self.taxed_amount = taxed_amount
        self.refund_amount = refund_amount
    
    @schema.validate_inputs
    def set_payment_extra_info(
        self,
        installments,
        invoice=None,
        indi=None,
        plan=None,
        iva_discount_amount=None,
    ):
        """
            Set the extra info relative to the payment
        """
        self.installments = installments
        self.invoice = invoice
        self.indi = indi
        self.plan = plan
        self.iva_discount_amount = iva_discount_amount

    @schema.validate_inputs
    def set_additional_data(
        self,
        additional_data
    ):
        self.additional_data = additional_data


class SecurityCode(Base):

    schema = schemas.SecurityCodeSchema()

    @schema.validate_inputs
    def __init__(self, type, value):
        self.type = type
        self.value = value


class PaymentResponse(ResponseBase):

    schema = schemas.PaymentResponseSchema()

    @schema.validate_inputs
    def __init__(
        self,
        response_header,
        token,
        payment=None,
        reference=None,
        ecommerce_additional_data=None,
        additional_data=None,
        security_code=None
    ):
        self.response_header = response_header
        self.token = token
        self.payment = payment
        self.reference = reference
        self.ecommerce_additional_data = ecommerce_additional_data
        self.additional_data = additional_data
        self.security_code = security_code

    @classmethod
    def new_from_response(cls, response_data):
        _data = tools.parse_dict_keys(response_data)

        response_header = ResponseHeader(**_data.get("response_header"))
        token = _data.get("token", None)
        reference = _data.get("reference", None)
        ecommerce_additional_data = _data.get("ecommerce_additional_data", None)
        additional_data = _data.get("additional_data", None)
        security_code = _data.get("security_code", None)
        payment = _data.get("payment", None)

        return cls(
            response_header,
            token,
            payment=payment,
            reference=reference,
            ecommerce_additional_data=ecommerce_additional_data,
            additional_data=additional_data,
            security_code=security_code
        )

    @classmethod
    def _get_payment_object(cls, payment_data):
        if not payment_data:
            return None

        payment = Payment(
            payment_data.get("payment_token"),
            payment_data.get("date_time"),
            payment_data.get("state"),
            payment_data.get("geopay_merchant_id"),
            access_token=payment_data.get("access_token", None),
            preauthorized=payment_data.get("preauthorized", None)
        )

        payment.set_amounts(
            payment_data.get("currency"),
            payment_data.get("amount"),
            tax_amount=payment_data.get("tax_amount", None),
            tip_amount=payment_data.get("tip_amount", None),
            taxed_amount=payment_data.get("taxed_amount", None),
            refund_amount=payment_data.get("refund_amount", None)
        )

        payment.set_payment_info(
            payment_data.get("paymentmode"),
            payment_data.get("payment_entity"),
            authorizer=payment_data.get("authorizer", None),
            authorized=payment_data.get("authorized", None),
            voided=payment_data.get("voided", None),
            refunded=payment_data.get("refunded", None)
        )

        payment.set_payment_extra_info(
            payment_data.get("installments"),
            invoice=payment_data.get("invoice", None),
            indi=payment_data.get("indi", None),
            plan=payment_data.get("plan", None),
            iva_discount_amount=payment_data.get("iva_discount_amount", None)
        )

        payment.set_additional_data(
            payment_data.get("additional_data", None)
        )

        payment.set_client(
            payment_data.get("client")
        )

        payment.set_card(
            payment_data.get("card")
        )

        return payment


class PaymentQuery(Base):

    schema = schemas.PaymentQuerySchema()

    @schema.validate_inputs
    def __init__(
        self,
        request_header
    ):
        self.request_header = request_header

    @schema.validate_inputs
    def set_filter_options(
        self,
        access_token=None,
        date_from=None,
        date_to=None,
        merchant_id=None,
        first_id=None
    ):
        """
            :param access_token: Used to query a specific payment
            where the token is the token returned in the RequestPaymentResponse
            :type access_token: str

            :param date_from: Start date to filter these payments with
            dates greater than equals to this. If there is no 'access_token' this is required.
            type: date_from: datetime

            :param date_to: End date to filter these payments with
            dates less than equals to this. If there is no 'access_token' this is required.
            type: date_from: datetime

            :param merchant_id: Used to filter a specific comerce, if you are working
            with multi-comerces
            :type merchant_id: str

            :param first_id: Position of the first element to be returned
            inside the respective list result, for the previous filters. (Pagination)
            :type first_id: int
        """

        if not access_token and not (date_from and date_to):
            raise ValueError(
                "You need to pass an access_token "
                "or the pair of date_from and date_to"
            )

        self.access_token = access_token
        self.date_from = date_from
        self.date_to = date_to
        self.merchant_id = merchant_id
        self.first_id = first_id


class PaymentQueryResponse(ResponseBase):

    schema = schemas.PaymentQueryResponseSchema()

    def __init__(
        self,
        response_header,
        payments=None,
        quantity=None,
        has_more=None,
        first_id=None,
        last_id=None
    ):
        self.response_header = response_header
        self.payments = payments
        self.quantity = quantity
        self.has_more = has_more
        self.first_id = first_id
        self.last_id = last_id
    
    @classmethod
    def new_from_response(cls, response_data):
        _data = tools.parse_dict_keys(response_data)
        response_header = ResponseHeader(**_data.get("response_header"))
        return cls(
            response_header,
            payments=_data.get("payments", None),
            quantity=_data.get("quantity", None),
            has_more=_data.get("has_more", None),
            first_id=_data.get("first_id", None),
            last_id=_data.get("last_id", None),
        )


class EchoTest(Base):

    schema = schemas.EchoTestSchema()

    @schema.validate_inputs
    def __init__(
        self,
        request_header
    ):
        self.request_header = request_header


class EchoTestResponse(ResponseBase):

    schema = schemas.EchoTestResponseSchema()

    @schema.validate_inputs
    def __init__(
        self,
        response_header
    ):
        self.response_header = response_header
    
    @classmethod
    def new_from_response(cls, response_data):
        _data = tools.parse_dict_keys(response_data)
        response_header = ResponseHeader(**_data.get("response_header"))
        return cls(response_header)