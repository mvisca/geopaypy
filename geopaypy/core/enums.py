from enum import Enum


class DocumentTypeEnum(Enum):
    CI = 1
    RUT = 2
    OTRO = 3

    @classmethod
    def available_values(cls):
        return [
            value.name for value in cls
        ]
            

class CountryEnum(Enum):
    URUGUAY = "UY"

    @classmethod
    def available_values(cls):
        return [
            value.name for value in cls
        ]


class CurrencyEnum(Enum):
    USD = "840"
    UYU = "858"

    @classmethod
    def available_values(cls):
        return [
            value.name for value in cls
        ]


class IndiEnum(Enum):
    NO = 0
    RESTAURANTS = 1
    IMESI = 2
    IVA = 6

    @classmethod
    def available_values(cls):
        return [
            value.name for value in cls
        ]


class PreAuthEnum(Enum):
    AGAINST_THE_SEAL = 0
    PRE_AUTHORIZE_OR_RETURN_CODE_AN = 2
    MANDATORY_PRE_AUTHORIZATION = 3
    PRE_AUTHORIZE_OR_BUY_AGAINST_THE_SEAL = 4


class VersionEnum(Enum):
    V12 = "1.2"
    V114 = "1.14"

    @classmethod
    def available_values(cls):
        return [
            value.name for value in cls
        ]


# class ResponseCodeEnum(Enum):

#     @classmethod
#     def available_values(cls):
#         return [
#             value.name for value in cls
#         ]

class PaymentStateEnum(Enum):
    PROCESSED = "PROCESSED"
    INPROCESS = "INPROCESS"
    UNKNOW_STATUS = "UNKNOW_STATUS"
    EXPIRED = "EXPIRED"
    CANCELLED = "CANCELLED"
    PREAUTHORIZED = "PREAUTHORIZED"
